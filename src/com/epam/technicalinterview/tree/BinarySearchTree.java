package com.epam.technicalinterview.tree;

public class BinarySearchTree<T extends Comparable<T>> extends BinaryTree<T> {
    public BinarySearchTree(){}
    public BinarySearchTree(T value){
        this.value = value;
    }

    public void add(T element){
        if (value == null) {
            value = element;
            return;
        }
        if (value.compareTo(element) < 0){
            if (right == null)
                right = new BinarySearchTree<>(element);
            else
                ((BinarySearchTree<T>)right).add(element);
        } else {
            if (left == null)
                left = new BinarySearchTree<>(element);
            else
                ((BinarySearchTree<T>)left).add(element);
        }
    }

    public boolean contains(T element){
        if (value.compareTo(element) < 0) {
            return right != null && ((BinarySearchTree<T>)right).contains(element);
        } else {
            return left != null && ((BinarySearchTree<T>)left).contains(element);
        }
    }

    private static <T extends Comparable<T>> boolean isBinary(BinaryTree<T> tree, T min, T max) {
        boolean betweenMinMax = (min == null || tree.value.compareTo(min) > 0) && (max == null || tree.value.compareTo(max) <= 0);
        if (!betweenMinMax)
            return false;
        boolean isBinaryLeft = tree.left == null || isBinary(tree.left, min, tree.value);
        return isBinaryLeft && (tree.right == null || isBinary(tree.right, tree.value, max));
    }

    public static <T extends Comparable<T>> boolean isBinary(BinaryTree<T> tree) {
        return isBinary(tree, null, null);
    }

    private static <T extends Comparable<T>> void printAllValues(T element, BinarySearchTree<T> tree){
        System.out.print(tree.value + " ");
        if (tree.right != null)
            printAllValues(element, (BinarySearchTree<T>)tree.right);
        if (tree.left != null)
            printAllValues(element, (BinarySearchTree<T>)tree.left);
    }

    private static <T extends Comparable<T>> void printLessValues(T element, BinarySearchTree<T> tree){
        if (tree.value.compareTo(element) < 0) {
            System.out.print(tree.value + " ");
            if (tree.right != null)
                printLessValues(element, (BinarySearchTree<T>)tree.right);
            if (tree.left != null)
                printAllValues(element, (BinarySearchTree<T>)tree.left);
        } else {
            if (tree.left != null)
                printLessValues(element, (BinarySearchTree<T>)tree.left);
        }
    }

    public void printLessValues(T element){
        printLessValues(element, this);
    }
}
