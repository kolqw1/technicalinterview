package com.epam.technicalinterview.tree;

public class BinaryTree<T extends Comparable<T>>  {
    protected T value;
    protected BinaryTree<T> left, right;

    public BinaryTree(){}
    public BinaryTree(T value){
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public BinaryTree<T> getOrCreateLeft() {
        if (left == null)
            left = new BinaryTree<>();
        return left;
    }

    public BinaryTree<T> getOrCreateRight() {
        if (right == null)
            right = new BinaryTree<>();
        return right;
    }

    public boolean isBalanced()
    {
        return height() - minHeight() <= 1;
    }

    public int height(){
        if ((left == null) && (right == null))
            return 0;
        else if (left == null)
            return 1 + right.height();
        else if (right == null)
            return 1 + left.height();
        else
            return 1 + Math.max(left.height(), right.height());
    }

    private int minHeight(){
        if ((left == null) && (right == null))
            return 0;
        else if (left == null)
            return 1 + right.height();
        else if (right == null)
            return 1 + left.height();
        else
            return 1 + Math.min(left.height(), right.height());
    }
}
