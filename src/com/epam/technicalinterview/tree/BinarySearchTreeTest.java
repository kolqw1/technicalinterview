package com.epam.technicalinterview.tree;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTreeTest {


    @Before
    public void setUp() throws Exception {

    }


    @Test
    public void testIsBinary() throws Exception {
        BinaryTree<Integer> tree = new BinaryTree<>();
        assertTrue(BinarySearchTree.isBinary(tree));
        tree.setValue(5);
        tree.getOrCreateLeft().setValue(5);
        tree.getOrCreateRight().setValue(6);
        tree.getOrCreateRight().getOrCreateRight().setValue(8);
        assertTrue(BinarySearchTree.isBinary(tree));
        tree.getOrCreateLeft().getOrCreateRight().setValue(6);
        assertFalse(BinarySearchTree.isBinary(tree));
        tree.getOrCreateLeft().getOrCreateRight().setValue(5);
        assertFalse(BinarySearchTree.isBinary(tree));
    }

    @Test
    public void testPrintLessValues() throws Exception {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(5);
        tree.add(3);
        tree.add(7);
        tree.add(2);
        tree.add(4);
        tree.add(6);
        tree.add(8);
        tree.add(9);
        tree.add(10);
        tree.printLessValues(5);
        System.out.println();
        tree.printLessValues(33);
        System.out.println();
        tree.printLessValues(10);
    }
}