package com.epam.technicalinterview.tree;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinaryTreeTest {
    BinarySearchTree<Integer> tree;
    @Before
    public void setUp() throws Exception {
        tree = new BinarySearchTree<>();
    }

    @Test
    public void test() throws Exception {
        tree.add(5);
        tree.add(3);
        tree.add(7);
        tree.add(2);
        tree.add(4);
        tree.add(6);
        tree.add(8);
        assertEquals(2, tree.height());
        assertTrue(tree.isBalanced());
        tree.add(9);
        assertEquals(3, tree.height());
        assertTrue(tree.isBalanced());
        tree.add(10);
        assertEquals(4, tree.height());
        assertFalse(tree.isBalanced());
    }
}