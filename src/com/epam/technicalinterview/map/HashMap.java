package com.epam.technicalinterview.map;

public class HashMap<K, V> {
    private K[] keyMass;
    private V[] valMass;
    private double loadFactor;
    private int size;
    private int capacity;
    HashMap() {
        this(20, 0.75);
    }

    HashMap(int capacity, double loadFactor) {
        if ((capacity <= 0) || (loadFactor <= 0) || (loadFactor > 1))
            throw new IllegalArgumentException();
        allocation(capacity);
        this.loadFactor = loadFactor;
    }

    public V get(K key){
        int num = key.hashCode() % capacity;
        while ((keyMass[num] != key) && (keyMass[num]!=null))
            num = (num + 1) % capacity;
        return valMass[num];
    }

    public void put(K key, V value){
        if (size + 1 >= loadFactor * capacity){
            rehash(capacity * 2);
        }
        int num = key.hashCode() % capacity;
        while ((keyMass[num] != null) && (!keyMass[num].equals(key)))
            num = (num + 1) % capacity;
        if (keyMass[num] == null)
            size++;
        keyMass[num] = key;
        valMass[num] = value;
    }

    public V remove(K key){
        int num = key.hashCode() % capacity;
        while ((keyMass[num] != key) && (keyMass[num] != null))
            num = (num + 1) % capacity;
        if (keyMass[num] != null)
            size--;
        V result = valMass[num];
        keyMass[num] = null;
        valMass[num] = null;
        return result;
    }

    public int size(){
        return size;
    }

    private void allocation(int capacity){
        size = 0;
        this.capacity = capacity;
        keyMass = (K[])new Object[capacity];
        valMass = (V[])new Object[capacity];
    }

    private void rehash(int capacity){
        K [] tmpKeyMass = keyMass;
        V [] tmpValMass = valMass;
        allocation(capacity);
        for (int i = 0; i < tmpKeyMass.length; i++)
            if (tmpKeyMass[i] != null)
                put(tmpKeyMass[i], tmpValMass[i]);
    }
}
