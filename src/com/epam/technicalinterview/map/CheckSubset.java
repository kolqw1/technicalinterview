package com.epam.technicalinterview.map;

import java.util.HashSet;

public class CheckSubset {
    //S = {s1, s2, ..., sm} and T = {t1, t2, ..., tn}, m ≤ n.
    //complexity O(m+n)
    public static boolean checkSubset(int[] S, int[] T)
    {
        HashSet<Integer> hashSet = new HashSet<>();
        for (int value : S) hashSet.add(value);
        for (int value : T) hashSet.add(value);
        return hashSet.size() == T.length;
    }

    public static void main(String[] args) {
        int [] m1 = {1, 2, 3, 4, 5};
        int [] m2 = {1, 2, 3};
        int [] m3 = {1, 2, 3, 6};
        System.out.println(checkSubset(m2, m1));
        System.out.println(checkSubset(m3, m1));
        System.out.println(checkSubset(m2, m3));
    }
}
