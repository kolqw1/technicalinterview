package com.epam.technicalinterview.map;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapTest {
    HashMap<Integer, Integer> hashMap;

    @Before
    public void setUp() throws Exception {
        hashMap = new HashMap<>();
    }

    @Test
    public void test() throws Exception {
        final int size = 20;
        for (int i = 0; i < size; i++)
            hashMap.put(i, 2*i);
        for (int i = 0; i < size; i++)
            assertEquals(2*i, (long)hashMap.get(i));
        for (int i = 0; i < size; i++)
            hashMap.put(i, i);
        for (int i = 0; i < size; i++)
            assertEquals(i, (long)hashMap.get(i));
        assertEquals(size, hashMap.size());
        for (int i = 0; i < size; i++)
            assertEquals(i, (long)hashMap.remove(i));
        for (int i = 0; i < size; i++)
            assertEquals(null, hashMap.get(i));
        assertEquals(0, hashMap.size());
    }
}