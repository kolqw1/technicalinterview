package com.epam.technicalinterview.queue;

import org.junit.Before;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class MinMaxQueueTest {
    class MyQueue extends ArrayDeque<Integer> implements MinMaxQueue<Integer>{}
    MyQueue myQueue;

    @Before
    public void setUp() throws Exception {
        myQueue = new MyQueue();
        myQueue.add(10);
        myQueue.add(15);
        myQueue.add(-5);
        myQueue.add(20);
        myQueue.add(0);
    }

    @org.junit.Test
    public void testMax() throws Exception {
        assertEquals(20, (long)myQueue.max());
    }

    @org.junit.Test
    public void testMin() throws Exception {
        assertEquals(-5, (long)myQueue.min());
    }
}