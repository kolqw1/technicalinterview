package com.epam.technicalinterview.queue;

import com.epam.technicalinterview.stack.Stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class QueueWithTwoStacks<T> {
    private Stack<T> stack[];
    private int stackNum;

    public QueueWithTwoStacks() {
        stack = new Stack[2];
        stack[0] = new Stack<>();
        stack[1] = new Stack<>();
        stackNum = 0;
    }

    private void changeOrder() {
        int stackNum2 = (stackNum + 1) % 2;
        while (stack[stackNum].size() > 0)
            stack[stackNum2].push(stack[stackNum].pop());
        stackNum = stackNum2;
    }

    public void add(T element) {
        if (stackNum == 1)
            changeOrder();
        stack[0].push(element);
    }

    public T remove() {
        if (stackNum == 0)
            changeOrder();
        return stack[1].pop();
    }

    public T element() {
        if (stackNum == 0)
            changeOrder();
        return stack[1].peek();
    }
}
