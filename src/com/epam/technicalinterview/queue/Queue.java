package com.epam.technicalinterview.queue;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Queue<T extends Comparable<T>> {
    private Object[] mass;
    private int pos = 0;
    private int length = 0;

    public Queue(){
        this(20);
    }
    public Queue(int reserved){
        mass = new Object[reserved];
    }

    public void add(T value)
    {
        if (length >= mass.length)
            mass = Arrays.copyOf(mass, mass.length*2);
        mass[pos + length] = value;
        length++;
    }

    public T remove()
    {
        if (length > 0) {
            T result = (T)mass[pos];
            pos = (pos + 1) % mass.length;
            length--;
            return result;
        } else
            throw new NoSuchElementException();
    }

    public T peek(){
        if (length > 0)
            return (T)mass[pos];
        else
            return null;
    }

    public int size(){
        return length;
    }

    public boolean isEmpty(){
        return length == 0;
    }

    private T minMax(int compare)
    {
        T minMax = (T)mass[pos];
        for (int i = pos + 1; i < pos + length; i++){
            if (minMax.compareTo((T)mass[i]) == compare)
                minMax = (T)mass[i];
        }
        return minMax;
    }

    public T max() {
        return minMax(-1);
    }

    public T min()
    {
        return minMax(1);
    }
}
