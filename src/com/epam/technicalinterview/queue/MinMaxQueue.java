package com.epam.technicalinterview.queue;

import java.util.Queue;

//solution without iterators and Collections.max/min methods
public interface MinMaxQueue<T extends Comparable<T>> extends Queue<T>  {
    default T _minmax(int compare)
    {
        T minmax = this.remove();
        this.add(minmax);
        for (int i = 0; i < this.size() - 1; i++){
            T element  = this.remove();
            if (minmax.compareTo(element) == compare)
                minmax = element;
            this.add(element);
        }
        return minmax;
    }

    default T max() {
        return _minmax(-1);
    }

    default T min()
    {
        return _minmax(1);
    }
}