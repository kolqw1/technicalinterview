package com.epam.technicalinterview.queue;

import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class QueueWithTwoStacksTest {
    QueueWithTwoStacks<Integer> queue;
    @Before
    public void setUp() throws Exception {
        queue = new QueueWithTwoStacks<>();
    }

    @Test
    public void test() throws Exception {
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        assertEquals(1, (long)queue.remove());
        assertEquals(2, (long)queue.remove());
        assertEquals(3, (long)queue.remove());
        assertEquals(4, (long)queue.element());
        assertEquals(4, (long)queue.remove());
        queue.add(6);
        queue.add(7);
        assertEquals(5, (long)queue.element());
        assertEquals(5, (long)queue.remove());
        assertEquals(6, (long)queue.remove());
    }

    @Test(expected = NoSuchElementException.class)
    public void testException() throws Exception {
        queue.add(1);
        queue.remove();
        queue.remove();
    }

}