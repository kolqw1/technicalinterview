package com.epam.technicalinterview.recursion;

import java.util.Arrays;

public class MaxValue {
    public static int max(int[] array){
        return max(array, 0);
    }

    public static int max(int[] array, int begin){
        if (array.length == begin + 1)
            return array[begin];
        else
            return Math.max(array[begin], max(array, begin + 1));
    }

    public static void main(String[] args) {
        int [] array = {4, 6, 2, 8, 3, 5, 1};
        System.out.println(max(array));
    }
}
