package com.epam.technicalinterview.recursion;

public class Digits {
    public static int digits(int num) {
        if (num < 10)
            return 1;
        else
            return 1 + digits(num / 10);
    }

    public static void main(String[] args) {
        System.out.println(digits(56789));
    }
}
