package com.epam.technicalinterview.recursion;

public class Fibonacci {
    public static void fib(int n, int[] mass)
    {
        if (n < 2)
            mass[n] = 1;
        else {
            if (mass[n-1] == 0)
                fib(n-1, mass);
            if (mass[n-2] == 0)
                fib(n-2, mass);
            mass[n] = mass[n - 1] + mass[n - 2];
        }
    }

    public static void main(String[] args) {
        final int num = 9;
        int[] mass = new int[num + 1];
        fib(num, mass);
        System.out.println(mass[num]);
    }
}
