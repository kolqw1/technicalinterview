package com.epam.technicalinterview.sorts;

import java.util.Arrays;

public class MergeSort {
    private static void sort(int[] source, int[] target, int begin, int end){
        if (begin != end) {
            int middle = (end - begin) / 2 + begin;
            sort(target, source, begin, middle);
            sort(target, source, middle + 1, end);
            int p = begin;
            int p1 = begin;
            int p2 = middle + 1;
            while (p <= end) {
                if (p2 > end){
                    target[p] = source[p1];
                    p1++;
                } else if (p1 > middle) {
                    target[p] = source[p2];
                    p2++;
                } else if (source[p1] < source[p2]){
                    target[p] = source[p1];
                    p1++;
                } else {
                    target[p] = source[p2];
                    p2++;
                }
                p++;
            }
        }
    }

    public static void sort(int[] mass){
        int[] target = Arrays.copyOf(mass, mass.length);
        sort(mass, target, 0, mass.length - 1);
        System.arraycopy(target, 0, mass, 0, target.length);
    }

    public static void main(String[] args) {
        int[] mass = {12, 13, 56, 78, 35, 23, 76, 49, 74, 12, 54, 45, 77, 33, 27, 85, 46, 12, 45, 67};
        sort(mass);
        System.out.println(Arrays.toString(mass));
    }
}
