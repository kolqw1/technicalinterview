package com.epam.technicalinterview.sorts;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class QuickSort {
    private static Random rand = new Random();

    private static void sort(int[] mass, int begin, int end) {
        if (begin >= end)
            return;
        int current = rand.nextInt(end - begin + 1 ) + begin;
        int left = 0;
        int right = current + 1;
        while ((left < current) && (right <= end)) {
            while ((left < current) && (mass[left] <= mass[current]))
                left++;
            while ((right <= end) && (mass[right] >= mass[current]))
                right++;
            if ((left < current) && (right <= end)) {
                int tmp = mass[left];
                mass[left] = mass[right];
                mass[right] = tmp;
            }
        }
        while (left < current) {
            if (mass[left] > mass[current]) {
                int tmp = mass[current];
                mass[current] = mass[left];
                current--;
                mass[left] = mass[current];
                mass[current] = tmp;
            } else
                left++;
        }
        while (right <= end) {
            if (mass[right] < mass[current]) {
                int tmp = mass[current];
                mass[current] = mass[right];
                current++;
                mass[right] = mass[current];
                mass[current] = tmp;
            } else
                right++;
        }
        sort(mass, begin, current - 1);
        sort(mass, current + 1, end);
    }

    public static void sort(int[] mass) {
        sort(mass, 0, mass.length - 1);
    }

    public static void main(String[] args) {
        int[] mass = {12, 13, 56, 78, 35, 23, 76, 49, 74, 12, 54, 45, 77, 33, 27, 85, 46, 12, 45, 67};
        sort(mass);
        System.out.println(Arrays.toString(mass));
    }
}
