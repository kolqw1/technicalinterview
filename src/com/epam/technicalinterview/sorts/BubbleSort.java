package com.epam.technicalinterview.sorts;

import java.util.Arrays;

public class BubbleSort {
    public static void sort(int[] mass){
        for (int i = 0; i < mass.length; i++){
            for (int j = i; j < mass.length; j++){
                if (mass[i] > mass [j]){
                    int tmp = mass[i];
                    mass[i] = mass[j];
                    mass[j] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] mass = {12, 13, 56, 78, 35, 23, 76, 49, 74, 12, 54, 45, 77, 33, 27, 85, 46, 12, 45, 67};
        sort(mass);
        System.out.println(Arrays.toString(mass));
    }
}
