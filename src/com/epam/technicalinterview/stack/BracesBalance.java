package com.epam.technicalinterview.stack;

import java.util.ArrayDeque;
import java.util.Deque;

public class BracesBalance {
    private static void printResult(boolean result)
    {
        if (result)
            System.out.println("OK!");
        else
            System.out.println("Error!");
    }

    private static boolean popAndCompare(Character ch, Stack<Character> stack){
        try {
            Character popped = stack.pop();
            if (popped != ch)
                return false;
        } catch(Exception e) {
            return false;
        }
        return true;
    }

    private static boolean isBalanced(CharSequence str){
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            Character current = str.charAt(i);
            switch (current) {
                case '{':
                case '[':
                case '(':
                    stack.push(current);
                    break;
                case '}':
                    if (!popAndCompare('{', stack))
                        return false;
                    break;
                case ']':
                    if (!popAndCompare('[', stack))
                        return false;
                    break;
                case ')':
                    if (!popAndCompare('(', stack))
                        return false;
                    break;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        printResult(isBalanced("({ab}c[d])"));
        printResult(isBalanced("({ab}c[d[])"));
    }
}
