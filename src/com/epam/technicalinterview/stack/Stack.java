package com.epam.technicalinterview.stack;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Stack<T> {
    private T[] mass;
    private int pos = 0;
    public Stack(){
        this(20);
    }
    public Stack(int reserved){
        mass = (T[])new Object[reserved];
    }

    public void push(T value)
    {
        if (pos >= mass.length)
            mass = Arrays.copyOf(mass, mass.length*2);
        mass[pos] = value;
        pos++;
    }

    public T pop()
    {
        if (pos > 0)
            return mass[--pos];
        else
            throw new NoSuchElementException();
    }

    public T peek(){
        if (pos > 0)
            return mass[pos - 1];
        else
            return null;
    }

    public int size(){
        return pos;
    }

    public boolean isEmpty(){
        return pos == 0;
    }
}
