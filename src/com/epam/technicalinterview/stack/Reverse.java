package com.epam.technicalinterview.stack;

import java.util.*;

public class Reverse {
    private static void reverse(CharSequence str){
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++)
            stack.push(str.charAt(i));
        for (int i = 0; i < str.length(); i++)
            System.out.print(stack.pop());
    }

    public static void main(String[] args) {
        reverse("abcd");
    }
}
